import { flattenDeep, map, concat } from 'lodash'

const visitAndCheck = (currentId, orderedComponent, order) => {
  const current = orderedComponent.findIndex(comp => comp.id === currentId)
  const next = orderedComponent[current + 1]
  const prev = orderedComponent[current - 1]

  cy.get('[data-testid="current-component"]')
    .contains(`Author Book - title-${currentId}`)
    .should('be.visible')

  if (prev) {
    cy.get('[data-testid="previous-component"]')
      .contains(`title-${prev.id}`)
      .should('be.visible')
  } else {
    cy.get('[data-testid="previous-component"]').should('not.be.visible')
  }
  if (next) {
    cy.get('[data-testid="next-component"]')
      .contains(`title-${next.id}`)
      .should('be.visible')
  } else {
    cy.get('[data-testid="next-component"]').should('not.be.visible')
  }

  if (order === 'next' && next) {
    cy.get(`a:contains("title-${next.id}")`)
      .should('be.visible')
      .click()
    /* eslint-disable */
    cy.wait(500)
    /* eslint-enable */
    visitAndCheck(next.id, orderedComponent, order)
  }

  if (order === 'previous' && prev) {
    cy.get(`a:contains("title-${prev.id}")`)
      .should('be.visible')
      .click()
    /* eslint-disable */
    cy.wait(500)
    /* eslint-enable */
    visitAndCheck(prev.id, orderedComponent, order)
  }

  return true
}

describe('WaxPubsweet', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/createBooksWithUsersAndTeams.js')
    cy.exec('node ./scripts/createBookComponents.js')
  })
  it('navigate within wax from the first to the last bookComponent', async () => {
    cy.login('admin')
    cy.getCollections().then(res => {
      const {
        body: {
          data: { getBookCollections },
        },
      } = res

      const bookId = getBookCollections[0].books[0].id
      cy.getBookComponents(bookId).then(result => {
        const {
          body: {
            data: { getBook },
          },
        } = result

        const orderedComponent = flattenDeep(
          concat([
            ...map(getBook.divisions, division => division.bookComponents),
          ]),
        )

        const currentId = orderedComponent[0].id
        cy.visit(`/books/${getBook.id}/bookComponents/${currentId}`)
        visitAndCheck(currentId, orderedComponent, 'next')
      })
    })
  })

  it('navigate within wax from the last to the first bookComponent', async () => {
    cy.login('admin')
    cy.getCollections().then(res => {
      const {
        body: {
          data: { getBookCollections },
        },
      } = res
      const bookId = getBookCollections[0].books[0].id
      cy.getBookComponents(bookId).then(result => {
        const {
          body: {
            data: { getBook },
          },
        } = result

        const orderedComponent = flattenDeep(
          concat([
            ...map(getBook.divisions, division => division.bookComponents),
          ]),
        )
        const lastIndex = orderedComponent.length
        const currentId = orderedComponent[lastIndex - 1].id
        cy.visit(`/books/${getBook.id}/bookComponents/${currentId}`)
        visitAndCheck(currentId, orderedComponent, 'previous')
      })
    })
  })
})
