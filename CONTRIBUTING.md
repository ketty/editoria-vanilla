# How to write new E2E tests

Editoria uses Cypress to run end to end tests.

Follow the steps below to get up and running:

Make sure your test db is running:
If you have already run `yarn start:services` you should have two Postgres Docker containers running (one for development and one for testing). It might be good to look at the `docker-compose.yml` file in the application’s root folder. Worth noting is that the test db doesn’t have a data folder, as no data is meant to be persisted.
Create and source a `test.env` file (by convention inside the config folder). This should be the same as the development environment file, apart from the following values:

```
NODE_ENV=’test’
POSTGRES_DB=’test’
POSTGRES_USER=’test’
POSTGRES_PASSWORD=’pass’
POSTGRES_PORT=’5433’
```

Once you’ve sourced the `test.env` file, run `yarn setupdb` and create an admin account with username `admin` and password `password`. If you’ve done this step once for the testing db, you don’t need to do it again.
Bring up the server with `yarn server`.
On another terminal window / tab, run `yarn cypress`. This should bring the Cypress app up.
You can run the existing tests by clicking on the “Run all tasks” button at the top right. The tests will run in a browser window.

To make a new test, simply create a new file (eg. `mytest_spec.js`) inside the Cypress/integration folder on your app. You can use the rest of the files in there for reference. If you need to test a page that needs an authenticated user, use the login command to sign in as a particular role (eg. cy.login(‘author’). Should you need to seed data before your tests, you can execute a seed script in a before hook (eg. `cy.exec(filePath)`). You can take a look at `dashboard_spec.js` for an example of this.

You’re all set!

# Detailed instructions on seeding the database before running a test

In order to test your app you need to have data in your database. That means before you run your tests you should seed your database. 

You generally have three ways to facilitate this with Cypress:

* `cy.exec()` - to run system commands
* `cy.task()` - to run code in Node.js via the pluginsFile
* `cy.request()` - to make HTTP requests

In Editoria we are going to use the first approach. We are going to create scripts that can populate the database with data or scripts that truncate the database and then we executed with `cy.exec()`.

All we need is creating the script that we want to run for each test and call it before each test.

## Dashboard example

An example test could be a dashboard test, where certain books need to show up and be assigned to certain users.

So first thing is to create a script called e.g. `createBooksWithUsersAndTeams.js` which creates books and assigns’ them to admin and author user: https://gitlab.coko.foundation/editoria/ucp/blob/cypress/scripts/createBooksWithUsersAndTeams.js

And last but not least we should call that script by using ‘before’ and ‘beforeEach’ functions of Cypress like the example below.

```
beforeEach(() => {
   cy.exec('node ./scripts/truncateDB.js')
   cy.exec('node ./scripts/createBooksWithUsersAndTeams.js')
 })
```
