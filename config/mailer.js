module.exports = {
  transport: {
    host: 'smtp_server_placeholder',
    auth: {
      user: 'username_placeholder',
      pass: 'super_password',
    },
  },
}
